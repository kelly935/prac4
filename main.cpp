#include <iostream>
#include <sstream>

#include "Reverse.h"
#include "TruckLoads.h"
#include "EfficientTruckLoads.h"

bool isNotNumber(std::string num_string)
{
    if(num_string.size() <= 0)
    {
        return false;
    }
    
    for(unsigned int i = 0; i < num_string.size(); i++)
    {
        if(!std::isdigit(num_string[i]))
        {
            return false;
        }
    }

    return true;
}

int main()
{
    std::string input, digit_string, string, num_crates_string, load_size_string;
    //Get a single line from cin and store in input string
    getline(std::cin, input);

    // Setup a string stream to more easily separate out input values
    std::stringstream stringstream(input);
    stringstream >> digit_string >> string >> num_crates_string >> load_size_string;

    int digit, num_crates, load_size;
    Reverse r;
    Truckloads t;
    EfficientTruckloads e;

    if(!isNotNumber(digit_string))
    {
        std::cout << "ERROR";
    }
    else
    {
        digit = stoi(digit_string);
        if(digit < 0)
        {
            std::cout << "ERROR";
        }
        else
        {
            std::cout << r.reverseDigit(digit);
        }
    }

    std::cout << " " << r.reverseString(string);
    
    if(!isNotNumber(num_crates_string) || !isNotNumber(load_size_string))
    {
        std::cout << " " << "ERROR ERROR" << std::endl;
    }
    else
    {
        num_crates = stoi(num_crates_string);
        load_size = stoi(load_size_string);
        if(num_crates < 1 || load_size < 1)
        {
            std::cout << " " << "ERROR ERROR" << std::endl;
        }
        else
        {
            std::cout << " " << t.numTrucks(num_crates, load_size);
            std::cout << " " << e.numTrucks(num_crates, load_size) << std::endl;
        }
    }
}