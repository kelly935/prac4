#include <iostream>
#include "TruckLoads.h"

int Truckloads::numTrucks(int numCrates, int loadSize)
{
    //Checks to make sure that the number of crates and loadsize are reasonable
    if(numCrates <= 0 || loadSize <= 0)
    {
        return 0;
    }

    //Base case
    if(numCrates <= loadSize)
    {
        return 1;
    }

    /** Divide the number of crates into piles if the number of creates is odd
     *  create one pile with an extra crate.
     */ 
    int pile = numCrates / 2;
    if(numCrates % 2 == 0)
    {
        return numTrucks(pile, loadSize) + numTrucks(pile, loadSize);
    }
    else
    {
        return numTrucks(pile + 1, loadSize) + numTrucks(pile, loadSize);
    }
}