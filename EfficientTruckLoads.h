#ifndef _EFFICIENT_TRUCKLOADS_H_
#define _EFFICIENT_TRUCKLOADS_H_

#include <map>

class EfficientTruckloads
{
    public:
        int numTrucks(int numCrates, int loadSize);
    private:
        bool isMapped(int numCrates);
        std::map<int,int> calcs;
};

#endif