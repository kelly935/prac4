#include <iostream>
#include "Reverse.h"

Reverse::Reverse()
{

}

/** Gets the multiplier we need when reversing the digits based on the size of the current
* Number in the recursive function. ie 5 * 10000, 4 * 1000 ....
*/
int Reverse::get_index(int i)
{
    int index = 1;
    for(int j = 0; j < i; j++)
    {
        index *= 10;
    }
    return index;
}

/** Recusively gets the remainder of the number divided by 10 mutliplied by the index (see above)
 *  The next number (removing the last digit) is done by using the fact that an integer divide
 *  is always rounded down in C++.
 */
int Reverse::reverseDigit(int value)
{
    if(value == 0)
    {
        return 0;
    }

    int rem = value % 10;
    int next_num = value / 10;

    int i = 1;
    while(value % get_index(i) != value)
    {
        i++;
    }

    return rem*get_index(i-1) + reverseDigit(next_num);
}

/** Self explanitory - get the last letter of the string and pop that letter off the string before
 *  parsing it to the recursive function
 */
std::string Reverse::reverseString(std::string letters)
{
    if(letters.size() > 0)
    {
        std::string back_letter;
        back_letter = letters.back();
        letters.pop_back();
        return back_letter + reverseString(letters);
    }
    else
        return "";
}
