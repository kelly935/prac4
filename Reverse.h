#ifndef _REVERSE_H_
#define _REVERSE_H_

#include <string>

class Reverse
{
    public:
        Reverse();
        int reverseDigit(int value);
        std::string reverseString(std::string letters);
    private:
        int get_index(int i);
};

#endif