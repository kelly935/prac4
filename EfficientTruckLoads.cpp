#include <iostream>
#include "EfficientTruckLoads.h"

int EfficientTruckloads::numTrucks(int numCrates, int loadSize)
{
    //Checks to make sure that the number of crates and loadsize are reasonable
    if(numCrates <= 0 || loadSize <= 0)
    {
        return 0;
    }

    //Base case
    if(numCrates <= loadSize)
    {
        return 1;
    }


    /** Divide the number of crates into piles if the number of creates is odd
     *  create one pile with an extra crate.
     */ 
    int pile = numCrates / 2;
    int first, second;
    if(numCrates % 2 == 0)
    {
        /** Check if the pile number has been calculated before, is so use that rather than
         *  calling the recusive function
         */ 
        if(isMapped(pile))
        {
            return calcs[pile]*2;
        }

        first = numTrucks(pile, loadSize);
        second = numTrucks(pile, loadSize);

        // Make sure to add calculation to map if not already done before
        calcs[pile] = first;
    }
    else
    {
        //Check if mapped (see above)
        if(isMapped(pile + 1))
        {
            first = calcs[pile + 1];
        }
        else
        {
            first = numTrucks(pile + 1, loadSize);
            // Make sure to add calculation to map if not already done before
            calcs[pile + 1] = first;
        }
        //Check if mapped (see above)
        if(isMapped(pile))
        {
            second = calcs[pile];
        }
        else
        {
            second = numTrucks(pile, loadSize);
            // Make sure to add calculation to map if not already done before
            calcs[pile] = second;
        }
    }

    //Get the result of both pile calculations (either from map or calling recursive function)
    return first + second;
}

// Helper function for checking if the pile number has been calculated before
bool EfficientTruckloads::isMapped(int numCrates)
{
    //Auto makes compiler figure out type of iterator instead of me needing to figure it out
    auto itr = calcs.find(numCrates);
    
    if (itr != calcs.end())
        return true;

    return false;
}